/*

main.ts - execute functions for outputting to the Fediverse
Copyright (C) 2020  William R. Moore <caranmegil@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import generator, { Response, OAuth, Entity } from 'megalodon'
import request from 'superagent'
import moment from 'moment'

const SCOPES: Array<string> = ['read', 'write', 'follow']
const BASE_URL: string = `https://${process.env.HOST}` || 'https://chilli.social'

let accessToken: string | null
let refreshToken: string | null

const fs = require('fs')
const client = generator('pleroma', BASE_URL)

let config = JSON.parse(fs.readFileSync(process.env.CONFIG))
let code = config.code
let clientId = config.clientId
let secret = config.clientSecret

client
  .registerApp('aethred', {
    scopes: SCOPES
  })
  .then(appData => {
    console.log('Authorization URL is generated.')
    console.log(appData.url)
    console.log()
    return client.fetchAccessToken(clientId, secret, code)
  })
  .then((tokenData: OAuth.TokenData) => {
    accessToken = tokenData.accessToken
    refreshToken = tokenData.refreshToken

    setInterval( () => {
      if( refreshToken != null ) {
        client.refreshToken(clientId, secret, refreshToken).then((tokenData: OAuth.TokenData) => {
          accessToken = tokenData.accessToken
          refreshToken = tokenData.refreshToken
          activeClient = generator('pleroma', `https://${process.env.HOST}`, accessToken)
        })
      }
    }, 500000)

    let next_available = moment()

    let activeClient = generator('pleroma', `https://${process.env.HOST}`, accessToken)
    setInterval( () => {
      activeClient.getPublicTimeline().then( (resp: Response<Array<Entity.Status>>) => {
        resp.data.forEach( (value: any) => {
          let created_at = moment(value.created_at, "YYYY-MM-DDTHH:mm:ss.SSSZ")
          if (next_available.isBefore(created_at)) {
            next_available = created_at
            let hazardMatch = value.content.match(/\!hazard ([5-9])/)
            if (hazardMatch) {
              request
                .get(`${process.env.HAZARD_HOST}${hazardMatch[1]}`)
                .then( (resl) => {
                  switch (resl.body.result) {
                    case 0:
                      if (process.env.HAZARD_DEBUG && process.env.HAZARD_DEBUG === 'true') {
                        activeClient.postStatus(resl.body.log, {in_reply_to_id: value.id})
                      }
                      activeClient.postStatus('Congratulations!  You have nicked it!', {in_reply_to_id: value.id})
                      break;
                    case 1:
                      if (process.env.HAZARD_DEBUG && process.env.HAZARD_DEBUG === 'true') {
                        activeClient.postStatus(resl.body.log, {in_reply_to_id: value.id})
                      }
                      activeClient.postStatus('Sorry.  You are out!', {in_reply_to_id: value.id})
                      break;
                    default:
                      activeClient.postStatus('Something went wrong.', {in_reply_to_id: value.id})
                      break;
                  }
                })
                .catch( (err) => {
                  console.error(err)
                })
            } else {
              request
                .get(encodeURI(`${process.env.PERMISSIONS_HOST}/fediverse/${value.account.acct}`))
                .then( (res) => {
                  let permissions = res.body.results
                  if ( (permissions.indexOf("master") > -1 || permissions.indexOf("commander") >= -1) && value.content.includes(`${process.env.NAME}`)) {
                      request
                        .post(`${process.env.LINGUA_HOST}`)
                        .send( {text: value.content} )
                        .then( (resl) => {
                          resl.body.response.forEach( (resp: string) => {
                            activeClient.postStatus(resp, {in_reply_to_id: value.id})
                            .then( (res) => {
                              console.log(res)
                            })
                            .catch( (err) => {
                              console.error(err)
                            })
                          })
                        })
                        .catch( (err) => {
                          console.error(err)
                        })       
                    }
                })
                .catch( (err) => {
                  console.error(err)
                })
            }
          }
        })
      })
    }, 1000)
  })
  .catch((err: Error) => console.error(err))


// app.use(router.routes())
//    .use(router.allowedMethods())

// app.listen(process.env.PORT || 4000, () => {
//   console.log('Koa started at ' + moment().format('MM/DD/YYYY HH:mm') )
// }) 
