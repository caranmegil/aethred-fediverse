"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var megalodon_1 = __importDefault(require("megalodon"));
var superagent_1 = __importDefault(require("superagent"));
var moment_1 = __importDefault(require("moment"));
var SCOPES = ['read', 'write', 'follow'];
var BASE_URL = "https://" + process.env.HOST || 'https://chilli.social';
var accessToken;
var refreshToken;
var fs = require('fs');
var client = megalodon_1.default('pleroma', BASE_URL);
var config = JSON.parse(fs.readFileSync(process.env.CONFIG));
var code = config.code;
var clientId = config.clientId;
var secret = config.clientSecret;
client
    .registerApp('aethred', {
    scopes: SCOPES
})
    .then(function (appData) {
    console.log('Authorization URL is generated.');
    console.log(appData.url);
    console.log();
    return client.fetchAccessToken(clientId, secret, code);
})
    .then(function (tokenData) {
    accessToken = tokenData.accessToken;
    refreshToken = tokenData.refreshToken;
    setInterval(function () {
        if (refreshToken != null) {
            client.refreshToken(clientId, secret, refreshToken).then(function (tokenData) {
                accessToken = tokenData.accessToken;
                refreshToken = tokenData.refreshToken;
                activeClient = megalodon_1.default('pleroma', "https://" + process.env.HOST, accessToken);
            });
        }
    }, 500000);
    var next_available = moment_1.default();
    var activeClient = megalodon_1.default('pleroma', "https://" + process.env.HOST, accessToken);
    setInterval(function () {
        activeClient.getPublicTimeline().then(function (resp) {
            resp.data.forEach(function (value) {
                var created_at = moment_1.default(value.created_at, "YYYY-MM-DDTHH:mm:ss.SSSZ");
                if (next_available.isBefore(created_at)) {
                    next_available = created_at;
                    var hazardMatch = value.content.match(/\!hazard ([5-9])/);
                    if (hazardMatch) {
                        superagent_1.default
                            .get("" + process.env.HAZARD_HOST + hazardMatch[1])
                            .then(function (resl) {
                            switch (resl.body.result) {
                                case 0:
                                    if (process.env.HAZARD_DEBUG && process.env.HAZARD_DEBUG === 'true') {
                                        activeClient.postStatus(resl.body.log, { in_reply_to_id: value.id });
                                    }
                                    activeClient.postStatus('Congratulations!  You have nicked it!', { in_reply_to_id: value.id });
                                    break;
                                case 1:
                                    if (process.env.HAZARD_DEBUG && process.env.HAZARD_DEBUG === 'true') {
                                        activeClient.postStatus(resl.body.log, { in_reply_to_id: value.id });
                                    }
                                    activeClient.postStatus('Sorry.  You are out!', { in_reply_to_id: value.id });
                                    break;
                                default:
                                    activeClient.postStatus('Something went wrong.', { in_reply_to_id: value.id });
                                    break;
                            }
                        })
                            .catch(function (err) {
                            console.error(err);
                        });
                    }
                    else {
                        superagent_1.default
                            .get(encodeURI(process.env.PERMISSIONS_HOST + "/fediverse/" + value.account.acct))
                            .then(function (res) {
                            var permissions = res.body.results;
                            if ((permissions.indexOf("master") > -1 || permissions.indexOf("commander") >= -1) && value.content.includes("" + process.env.NAME)) {
                                superagent_1.default
                                    .post("" + process.env.LINGUA_HOST)
                                    .send({ text: value.content })
                                    .then(function (resl) {
                                    resl.body.response.forEach(function (resp) {
                                        activeClient.postStatus(resp, { in_reply_to_id: value.id })
                                            .then(function (res) {
                                            console.log(res);
                                        })
                                            .catch(function (err) {
                                            console.error(err);
                                        });
                                    });
                                })
                                    .catch(function (err) {
                                    console.error(err);
                                });
                            }
                        })
                            .catch(function (err) {
                            console.error(err);
                        });
                    }
                }
            });
        });
    }, 1000);
})
    .catch(function (err) { return console.error(err); });
